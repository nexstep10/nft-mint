
import {
  Connection,
  Keypair,
  Signer,
  PublicKey,
  Transaction,
  TransactionSignature,
  ConfirmOptions,
  sendAndConfirmRawTransaction,
  RpcResponseAndContext,
  SimulatedTransactionResponse,
  Commitment,
  LAMPORTS_PER_SOL,
  SYSVAR_RENT_PUBKEY
} from "@solana/web3.js";
import * as splToken from '@solana/spl-token'
import fs from 'fs'
import * as anchor from '@project-serum/anchor'

const sleep = (ms : number) => {
    return new Promise(resolve => setTimeout(resolve, ms));
};

export let programId = new PublicKey('2a7b125NsZNf4mkFvJJKH1JCUzTqNAEmzWvHUhrZhWrR')
let metadataProgramId = new PublicKey('metaqbxxUerdq28cj1RbAWkYQm3ybzjb6a8bt518x1s')
const idl=JSON.parse(fs.readFileSync('src/solana_anchor.json','utf8'))

export async function getAssociateTokenAddress(mint: any, owner: any) {
    let [address] = await PublicKey.findProgramAddress(
    [owner.toBuffer(), splToken.TOKEN_PROGRAM_ID.toBuffer(), mint.toBuffer()],
    splToken.ASSOCIATED_TOKEN_PROGRAM_ID,
    );
    return address;
}

async function createTokenAccount(conn : Connection, mint: PublicKey, wallet: Keypair) {
    let ata = await getAssociateTokenAddress(mint, wallet.publicKey)
    // console.log(ata.toBase58())
    if(await conn.getAccountInfo(ata)) {return ata;}
    let token = new splToken.Token(conn,mint,splToken.TOKEN_PROGRAM_ID,wallet)
    let tokenAccount = await token.createAssociatedTokenAccount(wallet.publicKey)
    return tokenAccount

}

export async function initPool(
    conn : Connection,
    owner : Keypair,
    sale_mint : PublicKey,
    ){
    const pool = Keypair.generate()
    let wallet = new anchor.Wallet(owner)
    let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
    const program = new anchor.Program(idl,programId,provider)
    const [presaleOwner,bump] = await PublicKey.findProgramAddress([pool.publicKey.toBuffer(),owner.publicKey.toBuffer()],programId)
    let token = new splToken.Token(conn,sale_mint,splToken.TOKEN_PROGRAM_ID,owner)
    let poolWallet = await createTokenAccount(conn,sale_mint,owner)
    // let token = await splToken.Token.createMint(conn,owner,owner.publicKey,null,9,splToken.TOKEN_PROGRAM_ID)
    // let poolWallet = await token.createAccount(owner.publicKey)
    try {
        await program.rpc.initPool(
            new anchor.BN(bump),
            {
                accounts:{
                   pool : pool.publicKey,
                   owner : owner.publicKey,
                   presaleOwner : presaleOwner,
                   saleMint : sale_mint,
                   poolWallet : poolWallet,
                   systemProgram : anchor.web3.SystemProgram.programId,
                },
                signers: [owner,pool] 
            }
        )
        console.log("Pool : "+pool.publicKey.toBase58())
        console.log("PoolWallet : "+poolWallet.toBase58())
    } catch(err) {
        console.log(err)
    }

    // const account = await program.account.pool.fetch(pool.publicKey)
    // console.log(account)
}

export async function setPresalePrice(
    conn : Connection,
    owner : Keypair,
    pool : PublicKey,
    price : number,
    ){
    let wallet = new anchor.Wallet(owner)
    let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
    const program = new anchor.Program(idl,programId,provider)
    try {
        await program.rpc.setPresalePrice(
            new anchor.BN(price),
            {
                accounts:{
                    owner : owner.publicKey,
                    pool : pool,
                }
            }
        )
    } catch(err){
        console.log(err)
    }
}

export async function setWhitelist(
    conn : Connection,
    pool : PublicKey,
    owner : Keypair,
    bidder : PublicKey,
    amount : number,
    whitelisted : Boolean
    ){
    let wallet = new anchor.Wallet(owner)
    let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
    const program = new anchor.Program(idl,programId,provider)
    let [client,bump] = await PublicKey.findProgramAddress(
        [programId.toBuffer(),pool.toBuffer(),bidder.toBuffer()],
        programId
    )

    try {
        await program.rpc.setWhitelist(
            new anchor.BN(bump),
            new anchor.BN(amount),
            {
                accounts:{
                   client : client,
                   pool : pool,
                   owner : owner.publicKey,
                   bidder : bidder,
                   systemProgram : anchor.web3.SystemProgram.programId,
                },
                signers: [owner] 
            }
        )
    } catch(err) {
        console.log(err)
    }
    // const account = await program.account.client.fetch(client)
    // console.log(account)
}

export async function isWhitelisted(
    conn : Connection,
    pool : PublicKey,
    account : PublicKey,
    ){
    let wallet = new anchor.Wallet(Keypair.generate())
    let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
    const program = new anchor.Program(idl,programId,provider)
    let [client,bump] = await PublicKey.findProgramAddress(
        [programId.toBuffer(),pool.toBuffer(),account.toBuffer()],
        programId
    )
    let accountData = await program.account.client.fetch(client)
    console.log(accountData.whitelisted)
    return accountData.whitelisted
}

export async function updateWhitelist(
    conn : Connection,
    pool : PublicKey,
    owner : Keypair,
    bidder : PublicKey,
    amount : number,
    whitelisted : Boolean
    ){
    let wallet = new anchor.Wallet(owner)
    let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
    const program = new anchor.Program(idl,programId,provider)
    let [client,bump] = await PublicKey.findProgramAddress(
        [programId.toBuffer(),pool.toBuffer(),bidder.toBuffer()],
        programId
    )

    try {
        await program.rpc.updateWhitelist(
            new anchor.BN(amount),
            whitelisted,
            {
                accounts:{
                   client : client,
                   pool : pool,
                   owner : owner.publicKey,
                },
                signers: [owner] 
            }
        )
    } catch(err) {
        console.log(err)
    }
    // const account = await program.account.client.fetch(client)
    // console.log(account)
}

export async function controlPresaleLive(
    conn : Connection,
    pool : PublicKey,
    owner : Keypair,
    isLive : Boolean,
    ){
    let wallet = new anchor.Wallet(owner)
    let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
    const program = new anchor.Program(idl,programId,provider)

    try {
        await program.rpc.controlPresaleLive(
            isLive,
            {
                accounts:{
                   pool : pool,
                   owner : owner.publicKey,
                }
            }
        )
    } catch(err) {
        console.log(err)
    }
    // const account = await program.account.pool.fetch(pool)
    // console.log(account)
}

export async function mintNft(
    conn : Connection,
    owner : Keypair,
    pool : PublicKey,
    data : any,
    ){
    let wallet = new anchor.Wallet(owner)
    let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
    const program = new anchor.Program(idl,programId,provider)
    let poolData = await program.account.pool.fetch(pool)
    let tokenMint = await splToken.Token.createMint(conn,owner,owner.publicKey,null,0,splToken.TOKEN_PROGRAM_ID)
    let mint = tokenMint.publicKey
    let tokenAccount = await tokenMint.createAccount(poolData.presaleOwner)
    let metadata = (await PublicKey.findProgramAddress([Buffer.from('metadata'),metadataProgramId.toBuffer(),mint.toBuffer()],metadataProgramId))[0]
    let master_endition = (await PublicKey.findProgramAddress([Buffer.from('metadata'),metadataProgramId.toBuffer(),mint.toBuffer(),Buffer.from('edition')],metadataProgramId))[0]
    let [metadata_extended,bump] = (await PublicKey.findProgramAddress([mint.toBuffer(),pool.toBuffer(),programId.toBuffer()],programId))
    try {
        await program.rpc.mintNft(
            data,
            {
                accounts:{
                   owner : owner.publicKey,
                   pool : pool,
                   mint : mint,
                   tokenAccount : tokenAccount,
                   metadata : metadata,
                   masterEdition : master_endition,
                   tokenMetadataProgram : metadataProgramId,
                   tokenProgram : splToken.TOKEN_PROGRAM_ID,
                   systemProgram : anchor.web3.SystemProgram.programId,
                   rent : SYSVAR_RENT_PUBKEY,
                },
            }
        )
        console.log("NFT address : "+mint.toBase58())
        console.log("account : "+tokenAccount.toBase58())
    } catch(err) {
        console.log(err)
    }
    // const account = await program.account.metadataExtended.fetch(metadata_extended)
    // console.log(account)
}

// export async function initSaleManager(
//     conn : Connection,
//     owner : Keypair,
//     pool : PublicKey,
//     nft_mint : PublicKey,
//     ){
//     console.log("+ initSaleManager")
//     let wallet = new anchor.Wallet(owner)
//     let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
//     const program = new anchor.Program(idl,programId,provider)
//     let [sale_manager,bump] = (await PublicKey.findProgramAddress([pool.toBuffer(),nft_mint.toBuffer()],programId))
//     try {
//         await program.rpc.initSaleManager(
//             new anchor.BN(bump),
//             {
//                 accounts:{
//                     owner : owner.publicKey,
//                     pool : pool,
//                     nftMint : nft_mint,
//                     saleManager : sale_manager,
//                     systemProgram : anchor.web3.SystemProgram.programId,
//                 },
//                 signers:[owner]
//             }
//         )
//     } catch(err){

//     }
//     console.log("- end")
//     await sleep(1000)
//     // const account = await program.account.saleManager.fetch(sale_manager)
//     // console.log(account)
// }

// export async function sellNft(
//     conn : Connection,
//     owner : Keypair,
//     pool : PublicKey,
//     nft_mint : PublicKey,
//     nft_seller_token : PublicKey,
//     nft_manager_token : PublicKey,
//     manager_pot : PublicKey,
//     price : number
//     ){
//     console.log("+ sellNft")
//     let wallet = new anchor.Wallet(owner)
//     let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
//     const program = new anchor.Program(idl,programId,provider)
//     let sale_manager = (await PublicKey.findProgramAddress([pool.toBuffer(),nft_mint.toBuffer()],programId))[0]
//     let metadata = (await PublicKey.findProgramAddress([Buffer.from('metadata'),metadataProgramId.toBuffer(),nft_mint.toBuffer()],metadataProgramId))[0]
//     let metadata_extended = (await PublicKey.findProgramAddress([nft_mint.toBuffer(),pool.toBuffer(),programId.toBuffer()],programId))[0]
//     let sale_pot = Keypair.generate()

//     try {
//         await program.rpc.sellNft(
//             new anchor.BN(price),
//             {
//                 accounts:{
//                     owner : owner.publicKey,
//                     pool : pool,
//                     nftMint : nft_mint,
//                     metadata : metadata,
//                     metadataExtended : metadata_extended,
//                     saleManager : sale_manager,
//                     salePot : sale_pot.publicKey,
//                     nftSellerToken : nft_seller_token,
//                     nftManagerToken : nft_manager_token,
//                     managerPot : manager_pot,
//                     tokenMetadataProgram : metadataProgramId,
//                     tokenProgram : splToken.TOKEN_PROGRAM_ID,
//                     systemProgram : anchor.web3.SystemProgram.programId,
//                 },
//                 signers:[owner,sale_pot]
//             }
//         )
//     } catch(err){
//         console.log(err)
//     }
//     console.log("- end")
//     await sleep(1000)
//     // const account = await program.account.saleManager.fetch(sale_manager)
//     // console.log(account)
// }

// export async function buyNft(
//     conn : Connection,
//     owner : Keypair,
//     pool : PublicKey,
//     nft_mint : PublicKey,
//     // nft_manager_token : PublicKey,
//     nft_bidder_token : PublicKey,
//     // manager_pot : PublicKey,
//     bidder_token : PublicKey,
//     ){
//     console.log("+ buyNft")
//     let wallet = new anchor.Wallet(owner)
//     let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
//     const program = new anchor.Program(idl,programId,provider)    
//     let sale_manager = (await PublicKey.findProgramAddress([pool.toBuffer(),nft_mint.toBuffer()],programId))[0]
//     let metadata = (await PublicKey.findProgramAddress([Buffer.from('metadata'),metadataProgramId.toBuffer(),nft_mint.toBuffer()],metadataProgramId))[0]
//     let sale_manager_data = await program.account.saleManager.fetch(sale_manager)
//     let sale_pot_data = await program.account.salePot.fetch(sale_manager_data.salePot)
//     try {
//         await program.rpc.buyNft(
//             {
//                 accounts:{
//                     owner : owner.publicKey,
//                     pool : pool,
//                     nftMint : nft_mint,
//                     metadata : metadata,
//                     saleManager : sale_manager,
//                     salePot : sale_manager_data.salePot,
//                     nftManagerToken : sale_manager_data.nftPot,
//                     nftBidderToken : nft_bidder_token,
//                     managerPot : sale_pot_data.poolPot,
//                     bidderToken : bidder_token,
//                     tokenMetadataProgram : metadataProgramId,
//                     tokenProgram : splToken.TOKEN_PROGRAM_ID,
//                 },
//                 signers:[owner]
//             }
//         )
//     } catch(err){
//         console.log(err)
//     }
//     console.log("- end")
//     await sleep(100)
//     const account = await program.account.saleManager.fetch(sale_manager)
//     console.log(account)
// }

// export async function redeemNft(
//     conn : Connection,
//     owner : Keypair,
//     pool : PublicKey,
//     nft_mint : PublicKey,
//     nft_seller_token : PublicKey,
//     ){
//     console.log("+ redeemNft")
//     let wallet = new anchor.Wallet(owner)
//     let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
//     const program = new anchor.Program(idl,programId,provider)    
//     let sale_manager = (await PublicKey.findProgramAddress([pool.toBuffer(),nft_mint.toBuffer()],programId))[0]
//     let metadata = (await PublicKey.findProgramAddress([Buffer.from('metadata'),metadataProgramId.toBuffer(),nft_mint.toBuffer()],metadataProgramId))[0]
//     let sale_manager_data = await program.account.saleManager.fetch(sale_manager)
//     try {
//         await program.rpc.redeemNft(
//             {
//                 accounts:{
//                     owner : owner.publicKey,
//                     pool : pool,
//                     nftMint : nft_mint,
//                     metadata : metadata,
//                     saleManager : sale_manager,
//                     nftSellerToken : nft_seller_token,
//                     nftManagerToken : sale_manager_data.nftPot,
//                     tokenMetadataProgram : metadataProgramId,
//                     tokenProgram : splToken.TOKEN_PROGRAM_ID,
//                 },
//                 signers:[owner]
//             }
//         )
//     } catch(err){
//         console.log(err)
//     }
//     console.log("- end")
//     await sleep(100)
//     // const account = await program.account.saleManager.fetch(sale_manager)
//     // console.log(account)    
// }

// export async function withdrawFund(
//     conn : Connection,
//     owner : Keypair,
//     sale_manager : PublicKey,
//     withraw_pot : PublicKey,
//     ){
//     console.log("+ withdrawFund")
//     let wallet = new anchor.Wallet(owner)
//     let provider = new anchor.Provider(conn,wallet,anchor.Provider.defaultOptions())
//     const program = new anchor.Program(idl,programId,provider)   
//     let sale_manager_data = await program.account.saleManager.fetch(sale_manager)
//     let sale_pot_data = await program.account.salePot.fetch(sale_manager_data.salePot)
//     try {
//         await program.rpc.withdrawFund(
//             {
//                 accounts:{
//                     owner : owner.publicKey,
//                     saleManager : sale_pot_data.saleManager,
//                     salePot : sale_manager_data.salePot,
//                     poolPot : sale_pot_data.poolPot,
//                     withdrawPot : withraw_pot,
//                     tokenProgram : splToken.TOKEN_PROGRAM_ID,
//                 },
//                 signers:[owner]
//             }
//         )
//     } catch(err){
//         console.log(err)
//     }
//     console.log("- end")
//     await sleep(100)
//     // const account = await program.account.saleManager.fetch(sale_manager)
//     // console.log(account)    
// }
