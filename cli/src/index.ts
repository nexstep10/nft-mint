import {
  Connection,
  Keypair,
  Signer,
  PublicKey,
  Transaction,
  TransactionSignature,
  ConfirmOptions,
  sendAndConfirmRawTransaction,
  RpcResponseAndContext,
  SimulatedTransactionResponse,
  Commitment,
  LAMPORTS_PER_SOL,
  clusterApiUrl
} from "@solana/web3.js";
import * as bs58 from 'bs58'
import * as splToken from '@solana/spl-token'
import fs from 'fs'
import * as anchor from '@project-serum/anchor'
import * as pool_api from './pool_api'
import { program } from 'commander';
import log from 'loglevel';

programCommand('init_pool')
    .option(
        '-c, --creator <string>',
        'nft market creator keypair',
        '2pUVo4mVSnebLyLmMTHgPRNbk7rgZki77bsYgbsuuQX9585N4aKNXWJRpyc98qnpgRKRH2hzB8VVnqeffurW39F4'
    )
    .option(
        '-sm, --sale-mint <string>',
        'sale mint address',
        'So11111111111111111111111111111111111111112'
    )
    .action(async (directory,cmd)=>{
        const {env,creator,saleMint} = cmd.opts()
        const conn = new Connection(clusterApiUrl(env))
        const creatorKeypair = Keypair.fromSecretKey(bs58.decode(creator))
        const saleMintAddress = new PublicKey(saleMint)
        await pool_api.initPool(conn,creatorKeypair,saleMintAddress)
    })

programCommand('set_presale_price')
    .option(
        '-o, --owner <string>',
        'nft market owner keypair',
        '2pUVo4mVSnebLyLmMTHgPRNbk7rgZki77bsYgbsuuQX9585N4aKNXWJRpyc98qnpgRKRH2hzB8VVnqeffurW39F4'
    )
    .option(
        '-m, --market <string>',
        'market pool address',
    )
    .option(
        '-p, --price <number>',
        'presale price',
        '1000000000'
    )
    .action(async (directory,cmd)=>{
        const {env,owner,market,price} = cmd.opts()
        const conn = new Connection(clusterApiUrl(env))
        const ownerKeypair = Keypair.fromSecretKey(bs58.decode(owner))
        const marketAddress = new PublicKey(market)
        await pool_api.setPresalePrice(conn,ownerKeypair,marketAddress,price)
    })

programCommand('set_whitelist')
    .option(
        '-o, --owner <string>',
        'nft market owner keypair',
        '2pUVo4mVSnebLyLmMTHgPRNbk7rgZki77bsYgbsuuQX9585N4aKNXWJRpyc98qnpgRKRH2hzB8VVnqeffurW39F4'
    )
    .option(
        '-m, --market <string>',
        'market pool address',
    )
    .option(
        '-b, --bidder  <string>',
        'address being whitelisted',
    )
    .option(
        '-a, --amount <number>',
        'presale amount for bidder',
        '3'
    )
    .action(async (directory,cmd)=>{
        const {env,owner,market,bidder,amount} = cmd.opts()
        const conn = new Connection(clusterApiUrl(env))
        const ownerKeypair = Keypair.fromSecretKey(bs58.decode(owner))
        const marketAddress = new PublicKey(market)
        const bidderAddress = new PublicKey(bidder)
        await pool_api.setWhitelist(conn, marketAddress, ownerKeypair, bidderAddress, amount, true)
    })

programCommand('get_whitelisted')
    .option(
        '-m, --market <string>',
        'market pool address',
    )
    .option(
        '-b, --bidder <string>',
        'address being whitelisted',
    )
    .action(async (directory,cmd)=>{
        const {env,market,bidder} = cmd.opts()
        const conn = new Connection(clusterApiUrl(env))
        const marketAddress = new PublicKey(market)
        const bidderAddress = new PublicKey(bidder)
        await pool_api.isWhitelisted(conn,marketAddress,bidderAddress)
    })

programCommand('set_presale')
    .option(
        '-o, --owner <string>',
        'nft market owner keypair',
        '2pUVo4mVSnebLyLmMTHgPRNbk7rgZki77bsYgbsuuQX9585N4aKNXWJRpyc98qnpgRKRH2hzB8VVnqeffurW39F4'
    )
    .option(
        '-m, --market <string>',
        'market pool address',
    )
    .option(
        '-s, --status <string>',
        'presale status : s : start, e : end',
        's'
    )
    .action(async (directory,cmd)=>{
        const {env,owner,market,status} = cmd.opts()
        const conn = new Connection(clusterApiUrl(env))
        const ownerKeypair = Keypair.fromSecretKey(bs58.decode(owner))
        const marketAddress = new PublicKey(market)
        await pool_api.controlPresaleLive(conn, marketAddress, ownerKeypair, status=='s' ? true : false)
    }) 

programCommand('mint_nft')
    .option(
        '-o, --owner <string>',
        'nft market owner keypair',
        '2pUVo4mVSnebLyLmMTHgPRNbk7rgZki77bsYgbsuuQX9585N4aKNXWJRpyc98qnpgRKRH2hzB8VVnqeffurW39F4'
    )
    .option(
        '-m, --market <string>',
        'market pool address',
    )
    .option(
        '-d, --data <string>',
        'data json url',
    )
    .action(async (directory,cmd)=>{
        const {env,owner,market,data} = cmd.opts()
        const conn = new Connection(clusterApiUrl(env))
        const ownerKeypair = Keypair.fromSecretKey(bs58.decode(owner))
        const marketAddress = new PublicKey(market)
        // const dataJson = JSON.parse(fs.readFileSync(data,'utf8'))
        let metadata = {
            name : 'nft',
            symbol : 'coff',
            uri : 'https://arweave.net/a03hkxJcMxG4DR-VtkE0WMMXL8-NWluV9-IU5RtMFKc',
            sellerFeeBasisPoints : 300, //3% (0 - 10000)
            creators : [
                {address: ownerKeypair.publicKey, verified:false, share:100}
            ],
            isMutable : true,
        }
        await pool_api.mintNft(conn, ownerKeypair, marketAddress, metadata)
    })

function programCommand(name: string) {
  return program
    .command(name)
    .option(
      '-e, --env <string>',
      'Solana cluster env name',
      'devnet',
    )
    .option('-l, --log-level <string>', 'log level', setLogLevel);
}

function setLogLevel(value : any, prev : any) {
  if (value === undefined || value === null) {
    return;
  }
  console.log('setting the log value to: ' + value);
  log.setLevel(value);
}

program.parse(process.argv)